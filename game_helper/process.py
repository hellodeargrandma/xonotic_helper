from pathlib import Path
from typing import Iterable, List, Optional
import logging
import pickle

import cv2
import numpy as np
from PIL import Image


class Model:
    def __init__(self, icov, mean):
        self._icov = icov
        self._mean = mean

    @property
    def icov(self):
        return icov

    @property
    def mean(self):
        return mean


class ColorSegmentation:
    def __init__(
        self, log: logging.Logger = logging.getLogger(Path(__file__).name)
    ):
        """ Object constructor.

        Initializes enemies searching algorithm -- teaches it or loads given
        model.

        Args:
            log: Logger to write logs to.
            model_path: Path to model. Save model there if gt_data_path given,
            load model from there otherwise.
            gt_data_path: Path to image[s] with Ground True pixels that has
            color of enemy.

        Return:
            Image with marked enemies.
        """
        self._log = log
        self._model: Optional[Model] = None

    def load_model(self, model_path: Path):
        """ Loads model from given path.

        Args:
            model_path: Path to file where to save model.

        Exceptions:
            Throws exception if model isn't loaded or not trained.
        """

        with open(model_path, "rb") as f:
            self._model = pickle.load(f)

    def dump_model(self, model_path: Path):
        """ Dumps trained model to given path.

        Args:
            model_path: Path to file where to save model.

        Exceptions:
            Throws exception if model isn't loaded or not trained.
        """

        if not self._model:
            raise Exception("Model isn't loaded.")

        with open(model_path, "wb") as f:
            pickle.dump(self._model, f)

    def train(self, path_to_gt: Path):
        """ Trains model to remember good pixels.

        Args:
            path_to_gt: Path to image[s] with Ground True good pixels. Pixels

        Exceptions:
            Throws exception if can't read given file.
        """
        samples = self.get_samples(path_to_gt)
        if not samples:
            self._log.warning("No samples read. Nothing to train.")
            return

        icov, mean = cv2.calcCovarMatrix(
            samples,
            [
                cv2.CV_COVAR_NORMAL,
                cv2.CV_COVAR_SCALE,
                cv2.CV_COVAR_ROWS,
                cv2.CV_64FC1,
            ],
        )
        icov = icov.inv(cv2.DECOMP_SVD)

        self._model = Model(icov, mean)

    def get_samples(self, path_to_gt: Path):
        """ Reads given file[s] and collects all non-zero pixels from there to
        a single array.

        Args:
            path_to_gt: Path to image[s] with Ground True good pixels. Pixels

        Return:
            An aray of RGB values of found GT pixels from all given files.
        """
        if path_to_gt.is_dir():
            samples: List[Iterable] = []
            for f in path_to_gt.iterdir():
                samples += self.get_samples(f)

            return samples

        img = None
        try:
            # TODO: couldn't read it with cv2.imread for some reason
            # img = cv2.imread(path_to_gt.absolute(), cv2.IMREAD_UNCHANGED)
            img = Image.open(path_to_gt)

        except Exception as ex:
            self._log.error(
                f"Unable to read file {path_to_gt.absolute()}: {ex}"
            )
            return []

        img_np = np.array(img)
        return img_np.reshape(-1, 3)

    def filter_pixels(self, img):
        """ Filters pixels that are not fit loaded model.

        Args:
            img: Image to filter.

        Return:
            A new filtered image.

        Exceptions:
            Throws exception if model isn't loaded or not trained.
        """

        if not self._model:
            raise Exception("Model isn't loaded.")

        return img

    def find_objects(self, img):
        """ Searches objects on given image.

        Args:
            img: Image to process.

        Return:
            A list of found objects.
        """

        self.filter_pixels(img)

        self._log.error("Not implemented.")
        return [(100, 100)]


class EnemiesMarker:
    def __init__(
        self,
        log: logging.Logger = logging.getLogger(Path(__file__).name),
        model_path: Optional[Path] = None,
        gt_data_path: Optional[Path] = None,
    ):
        """ Object constructor.

        Initializes enemies searching algorithm -- teaches it or loads given
        model.

        Args:
            log: Logger to write logs to.
            model_path: Path to model. Save model there if gt_data_path given,
            load model from there otherwise.
            gt_data_path: Path to image[s] with Ground True pixels that has
            color of enemy.
        """
        self._log = log
        self._algo = ColorSegmentation(log)
        if gt_data_path:
            self._algo.train(gt_data_path)
            if model_path:
                self._algo.dump_model(model_path)
        elif model_path:
            self._algo.load_model(model_path)
        else:
            raise Exception(
                "Processor class needs either path to model or path to GT data."
            )

    def process(self, frame):
        """ Processes given frame.

        Finds enemies on frame and marks them with red dot.

        Args:
            frame: Frame to be processed.

        Return:
            Image with marked enemies.
        """
        enemies = self._algo.find_objects()
        frame = self._mark_objects(enemies, frame)
        return frame

    def _mark_objects(objects, frame):
        for obj in objects:
            cv2.circle(
                img=frame, point=obj, radius=1, color=(255, 0, 0), thickness=-1
            )
