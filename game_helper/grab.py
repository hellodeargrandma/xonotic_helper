import re
import logging
from pathlib import Path
from typing import Union
from collections import namedtuple

import pyscreenshot
import numpy as np
import cv2

from Xlib.display import Display
from Xlib.xobject.drawable import Window
from Xlib.X import AnyPropertyType

Geom = namedtuple("Geom", "x y height width")


class Grabber:
    def __init__(
        self,
        wnd_name_regex: str,
        log: logging.Logger = logging.getLogger(Path(__file__).name),
    ):
        self._log = log
        self._display = Display()
        self._screen_root = self._display.screen().root
        self._wnd_name, self._wnd_obj = self._get_window(wnd_name_regex)
        self._wnd_bbox = ()
        self._update_window_bbox()

    def _get_window_name_inner(self, win_obj: Window) -> str:
        """Simplify dealing with _NET_WM_NAME (UTF-8) vs. WM_NAME (legacy)"""

        NET_WM_NAME = self._display.intern_atom("_NET_WM_NAME")  # UTF-8
        WM_NAME = self._display.intern_atom("WM_NAME")  # Legacy encoding

        for atom in (NET_WM_NAME, WM_NAME):
            try:
                window_name = win_obj.get_full_property(atom, 0)
            except UnicodeDecodeError:  # Apparently a Debian distro package bug
                title = "<could not decode characters>"
            else:
                if window_name:
                    win_name = window_name.value  # type: Union[str, bytes]
                    if isinstance(win_name, bytes):
                        # Apparently COMPOUND_TEXT is so arcane that this is how
                        # tools like xprop deal with receiving it these days
                        win_name = win_name.decode("latin1", "replace")
                    return win_name
                else:
                    title = "<unnamed window>"

        return title

    def _get_window(self, name_regex: str = ".*"):

        _NET_CLIENT_LIST = self._display.get_atom("_NET_CLIENT_LIST")

        client_list = self._screen_root.get_full_property(
            _NET_CLIENT_LIST, property_type=AnyPropertyType
        ).value

        p = re.compile(name_regex)
        for window_id in client_list:
            window = self._display.create_resource_object("window", window_id)
            name = self._get_window_name_inner(window)

            if p.match(name):
                return (name, window)

        raise Exception(f"No windows with names like {name_regex} found.")

    def _update_window_bbox(self):
        """
            Updates the (x, y, height, width) of a window relative to the
            top-left of the screen.
        """

        win = self._wnd_obj
        geom = win.get_geometry()
        (x, y) = (geom.x, geom.y)

        while True:
            parent = win.query_tree().parent
            pgeom = parent.get_geometry()
            x += pgeom.x
            y += pgeom.y
            if parent.id == self._screen_root.id:
                break
            win = parent

        bbox = Geom(x, y, x + geom.width, y + geom.height)

        if self._wnd_bbox != bbox:
            self._log.debug(bbox)
            self._wnd_bbox = bbox

    def get_frame(self):
        """ Returns a snapshot of window. """

        self._update_window_bbox()
        frame_pil = pyscreenshot.grab(
            bbox=self._wnd_bbox, backend="mss", childprocess=False
        )
        frame_np = np.array(frame_pil)
        frame_np = cv2.cvtColor(frame_np, cv2.COLOR_BGR2RGB)

        return frame_pil, frame_np
