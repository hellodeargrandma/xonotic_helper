#!/usr/bin/env python3

from datetime import datetime
from typing import Union
from pathlib import Path
import logging
from PIL import Image

import cv2
import configargparse
from game_helper.grab import Grabber
from game_helper.process import EnemiesMarker


def setup_logging(args: configargparse.Namespace, name: str) -> logging.Logger:
    """Set up logging module according to given parameters.

    Args:
        args: command line arguments parsed by argparse. This function
        waits following arguments to be present:
            "log_level": an enum of logging levels: "critical", "error",
                        "warn", "info", "debug".
        name: name of module that will be used as prefix in logs.
    Return:
        An logger object.

    """
    logger = logging.getLogger(name)
    logger.setLevel(logging.ERROR)

    if args.log_level:
        levels = {
            "critical": logging.CRITICAL,
            "error": logging.ERROR,
            "warn": logging.WARNING,
            "info": logging.INFO,
            "debug": logging.DEBUG,
        }
        logging.basicConfig(level=levels[args.log_level])
        logger.setLevel(levels[args.log_level])

    return logger


def parse_arguments() -> configargparse.Namespace:
    """Parses command line arguments using configargparse module."""

    parser = configargparse.ArgParser(
        description="Games helper.",
        config_file_parser_class=configargparse.YAMLConfigFileParser,
    )

    parser.add_argument(
        "-c", "--config", help="Path to config file.", is_config_file=True
    )

    parser.add_argument(
        "-ll",
        "--log-level",
        metavar="[l]",
        help=(
            "use level l for the logger (critical, error, warn, info, debug)"
        ),
        type=str,
        choices=["critical", "error", "warn", "info", "debug"],
    )

    window_opts = parser.add_argument_group("Window opts")
    window_opts.add(
        "-n",
        "--window-name",
        help="A regexp to find window by name.",
        default=".*",
    )

    io_opts = parser.add_argument_group("IO")

    io_opts.add(
        "-ds",
        "--dump-step",
        help="How often to dump frames. 0 means 'don't dump'",
        type=int,
        default=0,
    )

    io_opts.add(
        "-od",
        "--output-dir",
        help="Path to directory where to save output.",
        default="./out",
        type=Path,
    )

    io_opts.add(
        "-gt",
        "--gt-data",
        help="Path to directory where GT images stored.",
        type=Path,
    )

    return parser.parse_args()


def dump_frame(args: configargparse.Namespace, img: Image):
    fname = args.output_dir / Path(f"{datetime.timestamp(datetime.now())}.png")
    img.save(fname)


def main():
    args = parse_arguments()

    pr = EnemiesMarker(
        model_path=args.output_dir / Path("model"), gt_data_path=args.gt_data
    )
    gr = Grabber(args.window_name)
    dump_counter = 0

    while True:
        frame_pil, frame_np = gr.get_frame()
        processed = pr.process(frame_np)

        cv2.imshow("grabbed", frame_np)
        cv2.imshow("processed", processed)

        if args.dump_step:
            if dump_counter == 0:
                dump_frame(args, frame_pil)
                dump_counter = args.dump_step

            dump_counter -= 1

        if (cv2.waitKey(1) & 0xFF) == ord("q"):
            cv2.destroyAllWindows()
            break


if __name__ == "__main__":
    main()
