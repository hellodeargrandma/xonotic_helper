#!/usr/bin/env python3

import os
from distutils.core import setup
import setuptools

# Use project root to make out-of-source builds available.
project_root = os.path.dirname(__file__)

with open(os.path.join(project_root, 'README.md'), 'r') as f:
    long_description = f.read()

with open(os.path.join(project_root, 'requirements.txt'), 'r') as f:
    install_requires = [line.strip() for _, line in enumerate(f)]

print(install_requires)

entry_points = {'console_scripts': ['game_helper = game_helper.main:main']}

setup(
    name='game_helper',
    version='0.0.1',
    description='Should help you dominate.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    packages=setuptools.find_packages(),
    entry_points=entry_points,
    classifiers=[
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
    ],
    install_requires=install_requires,
    python_requires='>=3',
)
